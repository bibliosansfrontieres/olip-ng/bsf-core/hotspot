#!/bin/bash

SSID_PREFIX_FILE="/olip-files/deploy/ssid-prefix"
MAC_ID_FILE="/olip-files/network/mac-id"
SSID_FILE="/olip-files/deploy/ssid"
VM_SAVE_ID_FILE="/olip-files/deploy/virtual-machine-save-id"
HOSTAPD_CONF_FILE="/etc/hostapd/hostapd.conf"

ssid=""

say() {
    echo >&2 "[+] $*"
}

get_vm_save_id(){
    cat "$VM_SAVE_ID_FILE"
}

get_ssid_prefix(){
    cat "$SSID_PREFIX_FILE" 2>/dev/null || echo "ideascube"
}

get_mac_id() {
    cat "$MAC_ID_FILE"
}

get_ssid(){
    echo "$(get_ssid_prefix)_$(get_vm_save_id | tail -c 5)_$(get_mac_id)"
}

write_ssid(){
    ssid=$(get_ssid)
    temp_file=$(mktemp)
    sed "s/^ssid=.*/ssid=${ssid}/" "$HOSTAPD_CONF_FILE" > "$temp_file"
    cat "$temp_file" > "$HOSTAPD_CONF_FILE"

    # Écriture du SSID dans /olip-files/ssid
    echo "$ssid" > $SSID_FILE
}

write_ssid
say "Start hotspot \"${ssid}\"..."
exec "$@"