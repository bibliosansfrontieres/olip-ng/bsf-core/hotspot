# Before using

## Raspberry Pi OS Lite x64 (Bookworm)

```
# Set WIFI to FR
sudo raspi-config nonint do_wifi_country FR

# Remove wlan0 management by NetworkManager
echo "[keyfile]\nunmanaged-devices=interface-name:wlan0" | sudo tee /etc/NetworkManager/conf.d/wlan0.conf >/dev/null
sudo systemctl restart NetworkManager

# Fix IP address for wlan0
echo "auto wlan0\niface wlan0 inet static\n	address 10.3.141.1\n	netmask 255.255.255.0" | sudo tee /etc/network/interfaces.d/wlan0.conf >/dev/null
sudo systemctl restart networking
```

# Use with docker compose

## docker-compose.yml example

```
services:
  hotspot:
    image: offlineinternet/hotspot:latest
    container_name: bsf-core-hotspot
    network_mode: host
    restart: always
    privileged: true
    volumes:
      - ${HOSTAPD_CONFIG_FILE}:/etc/hostapd/hostapd.conf:ro
      - ${DNSMASQ_CONFIG_FILE}:/etc/dnsmasq.conf:ro
```