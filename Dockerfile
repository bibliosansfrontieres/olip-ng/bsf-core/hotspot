FROM alpine:3.19

# Install hostapd and dnsmasq
RUN apk add --no-cache \
    hostapd=2.10-r6 \
    dnsmasq=2.90-r2

WORKDIR /hotspot

COPY entrypoint.sh .

ENTRYPOINT ["sh", "./entrypoint.sh"]
    
# Run dnsmasq (in debug mode) and hostapd
CMD ["sh", "-c", "dnsmasq -d -k -C /etc/dnsmasq.conf & hostapd -d /etc/hostapd/hostapd.conf"]